import {createSlice, PayloadAction} from '@reduxjs/toolkit';

export interface AuthState {
  accessToken: any;
}

const initialState: AuthState = {
  accessToken: null,
};

export const authSlice = createSlice({
  name: 'userAuth',
  initialState,
  reducers: {
    loginUser: (state, action: PayloadAction<any>) => {
      state.accessToken = action.payload;
    },
    logoutUser: state => {
      state.accessToken = null;
    },
  },
});

export const {loginUser, logoutUser} = authSlice.actions;

export default authSlice.reducer;
