import {Keyboard, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React, {useState} from 'react';
import {colors} from '../../theme/colors';
import {size, weight} from '../../theme/fonts';
import Input from '../../components/Input';
import Email from '../../assets/images/svg/email.svg';
import Lock from '../../assets/images/svg/lock.svg';
import SmallLock from '../../assets/images/svg/small-lock.svg';
import {useDispatch} from 'react-redux';
import Loader from '../../components/Loader';

import {Formik} from 'formik';
import * as yup from 'yup';
import {SIGN_UP} from '../../constants/routeNames';
import CustomButton from '../../components/CustomButton';
import {storeAccessToken} from '../../utils/helpers';
import auth from '@react-native-firebase/auth';

const Login = ({navigation}) => {
  const dispatch = useDispatch();

  const [isLoading, setIsLoading] = useState(false);

  /** Form initial values */
  const initialValues = {email: '', password: ''};

  /** Form Validation */
  const validationSchema = yup.object().shape({
    email: yup
      .string()
      .nullable()
      .trim()
      .required('Please enter your email or user name.'),
    password: yup
      .string()
      .nullable()
      .trim()
      .required('Please enter your password.'),
  });

  /**  User login handle  */
  const handleLogin = async values => {
    const {email, password} = values;
    Keyboard.dismiss();
    setIsLoading(true);
    // const data = objectToFormData(values);
    try {
      const response = await auth().signInWithEmailAndPassword(email, password);

      // const response = await loginApi(data);
      // const fireBaseToken = await getFcmToken();
      // console.log(fireBaseToken, 'fireBaseToken');
      // const accessToken = response.data?.data?.token;
      // if (accessToken && response.data.status === 'success') {
      // const accessToken = '45678';
      await storeAccessToken(response.user.uid);
      // await dispatch(loginUser(accessToken));
      // } else {
      //   throw 'error';
      // }
      console.log(response.user.uid);

      setIsLoading(false);
    } catch (error: any) {
      console.log(error);

      setIsLoading(false);
    }
  };

  return (
    <View style={styles.container}>
      {isLoading && <Loader />}

      <Formik
        initialValues={initialValues}
        onSubmit={handleLogin}
        validationSchema={validationSchema}>
        {({
          values,
          errors,
          touched,
          handleBlur,
          handleChange,
          handleSubmit,
        }) => (
          <>
            <View style={styles.titleSection}>
              <Text style={styles.title}>Login to Your{'\n'}Account</Text>
            </View>
            <View style={styles.inputSection}>
              <Input
                label={'Email'}
                placeholder={'Email'}
                LeftIcon={Email}
                value={values.email}
                onChangeText={handleChange('email')}
                onBlur={handleBlur('email')}
                error={errors.email}
                touched={touched.email}
                wrapper={{marginVertical: 0}}
              />
              <Input
                label={'Password'}
                placeholder={'Password'}
                LeftIcon={Lock}
                value={values.password}
                onChangeText={handleChange('password')}
                onBlur={handleBlur('password')}
                error={errors.password}
                touched={touched.password}
                wrapper={{marginVertical: 0}}
              />
            </View>
            <View style={styles.buttonSection}>
              <TouchableOpacity
                style={styles.forgotPasswordButton}
                onPress={() => {
                  navigation.navigate(SIGN_UP);
                }}>
                <Text style={styles.forgotPasswordText}>
                  Don't have account sign up
                </Text>
              </TouchableOpacity>

              <CustomButton title={'Log in'} onPress={() => handleSubmit()} />

              <View style={styles.infoTextWrapper}>
                <SmallLock />
                <Text style={styles.infoText}>
                  {' '}
                  Your Info is safely secured
                </Text>
              </View>
            </View>
          </>
        )}
      </Formik>
    </View>
  );
};

export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    backgroundColor: colors.white,
    justifyContent: 'space-evenly',
  },
  titleSection: {
    flex: 0.2,
    justifyContent: 'flex-end',
  },
  inputSection: {
    flex: 0.5,
    justifyContent: 'center',
  },
  buttonSection: {
    flex: 0.3,
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  title: {
    color: colors.primary,
    fontWeight: weight.semi,
    fontSize: size.font28,
  },
  checkboxWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  forgotPasswordButton: {
    padding: 10,
  },
  forgotPasswordText: {
    fontSize: size.font12,
    color: colors.black,
    fontWeight: weight.semi,
  },
  infoTextWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  infoText: {
    color: colors.grey,
    fontSize: size.font10,
    fontWeight: weight.semi,
  },
  signUpButton: {
    paddingVertical: 4,
    paddingHorizontal: 4,
  },
  signUpButtonText: {
    color: colors.primary,
    fontWeight: weight.semi,
  },
});
