import {Keyboard, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React, {useState} from 'react';
import {colors} from '../../theme/colors';
import {size, weight} from '../../theme/fonts';
import Input from '../../components/Input';
import Email from '../../assets/images/svg/email.svg';
import Lock from '../../assets/images/svg/lock.svg';
import {useDispatch} from 'react-redux';
import Loader from '../../components/Loader';

import {Formik} from 'formik';
import * as yup from 'yup';
import CustomButton from '../../components/CustomButton';
import auth from '@react-native-firebase/auth';
import {LOGIN} from '../../constants/routeNames';

const SignUp = ({navigation}) => {
  const [isLoading, setIsLoading] = useState(false);

  /** Form initial values */
  const initialValues = {email: '', password: '', confirmPassword: ''};

  /** Form Validation */
  const validationSchema = yup.object().shape({
    email: yup
      .string()
      .nullable()
      .trim()
      .required('Please enter your email or user name.'),
    password: yup
      .string()
      .nullable()
      .trim()
      .required('Please enter your password.'),

    confirmPassword: yup
      .string()
      .nullable()
      .trim()
      .required('Please re-enter your new password.')
      .when('password', {
        is: val => (val && val.length > 0 ? true : false),
        then: yup
          .string()
          .oneOf([yup.ref('password')], 'Re-enter Password same as password'),
      }),
  });

  /**  User Sign Up handle  */
  const handleSignUp = async values => {
    const {email, password} = values;
    Keyboard.dismiss();
    setIsLoading(true);
    try {
      await auth().createUserWithEmailAndPassword(email, password);
      setIsLoading(false);
    } catch (error: any) {
      console.log(error);

      setIsLoading(false);
    }
  };

  return (
    <View style={styles.container}>
      {isLoading && <Loader />}

      <Formik
        initialValues={initialValues}
        onSubmit={handleSignUp}
        validationSchema={validationSchema}>
        {({
          values,
          errors,
          touched,
          handleBlur,
          handleChange,
          handleSubmit,
        }) => (
          <>
            <View style={styles.titleSection}>
              <Text style={styles.title}>Create New {'\n'}Account</Text>
            </View>
            <View style={styles.inputSection}>
              <Input
                label={'Email'}
                placeholder={'Email'}
                LeftIcon={Email}
                value={values.email}
                onChangeText={handleChange('email')}
                onBlur={handleBlur('email')}
                error={errors.email}
                touched={touched.email}
                wrapper={{marginVertical: 0}}
              />
              <Input
                label={'New Password'}
                placeholder={'New Password'}
                LeftIcon={Lock}
                value={values.password}
                onChangeText={handleChange('password')}
                onBlur={handleBlur('password')}
                error={errors.password}
                touched={touched.password}
                wrapper={{marginVertical: 0}}
              />
              <Input
                label={'Re-enter Password'}
                placeholder={'Re-enter Password'}
                LeftIcon={Lock}
                value={values.confirmPassword}
                onChangeText={handleChange('confirmPassword')}
                onBlur={handleBlur('confirmPassword')}
                error={errors.confirmPassword}
                touched={touched.confirmPassword}
                wrapper={{marginVertical: 0}}
              />
            </View>
            <View style={styles.buttonSection}>
              <TouchableOpacity
                style={styles.forgotPasswordButton}
                onPress={() => {
                  navigation.navigate(LOGIN);
                }}>
                <Text style={styles.forgotPasswordText}>Login</Text>
              </TouchableOpacity>

              <CustomButton title={'Sign Up'} onPress={() => handleSubmit()} />
            </View>
          </>
        )}
      </Formik>
    </View>
  );
};

export default SignUp;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    backgroundColor: colors.white,
    justifyContent: 'space-evenly',
  },
  titleSection: {
    flex: 0.2,
    justifyContent: 'flex-end',
  },
  inputSection: {
    flex: 0.6,
    justifyContent: 'center',
  },
  buttonSection: {
    flex: 0.2,
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  title: {
    color: colors.primary,
    fontWeight: weight.semi,
    fontSize: size.font28,
  },
  checkboxWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  forgotPasswordButton: {
    padding: 10,
  },
  forgotPasswordText: {
    fontSize: size.font12,
    color: colors.black,
    fontWeight: weight.semi,
  },
  infoTextWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  infoText: {
    color: colors.grey,
    fontSize: size.font10,
    fontWeight: weight.semi,
  },
  signUpButton: {
    paddingVertical: 4,
    paddingHorizontal: 4,
  },
  signUpButtonText: {
    color: colors.primary,
    fontWeight: weight.semi,
  },
});
