import {
  Image,
  Keyboard,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useState, useEffect, useRef} from 'react';

import {Formik} from 'formik';
import * as yup from 'yup';
import Loader from '../../components/Loader';
import commonStyle, {commonInputStyle} from '../../utils/commonStyle';
import Header from '../../components/Header';
import CustomButton from '../../components/CustomButton';
import {size} from '../../theme/fonts';
import {HOME} from '../../constants/routeNames';
import {colors} from '../../theme/colors';
import Input from '../../components/Input';
import firestore from '@react-native-firebase/firestore';
import {launchDeviceCameraLibrary} from '../../utils/helpers';
import CameraGalleryModal from '../../components/CameraGalleryModal';
import RippleButton from '../../components/RippleButton';

const AddProduct = ({navigation, route}) => {
  const {productId} = route.params || '';

  const {data} = route.params || {};

  const productsCollection = firestore().collection('products');

  const formRef: any = useRef();

  const [isLoading, setIsLoading] = useState(false);

  const [imageUri, setImageUri] = useState<any>(null);

  const [cameraGalleryVisible, setCameraGalleryVisible] = useState(false);

  /** Allow number and decimal 2 digit  **/
  const numberDecimalRegExp = RegExp(/^\d+(\.\d{0,2})?$/);

  /** Form initial values */
  const initialValues = {
    name: '',
    price: '',
    offerPrice: '',
  };

  /** Form Validation */
  const validationSchema = yup.object().shape({
    name: yup.string().nullable().trim().required('Please enter product name.'),
    price: yup.string().nullable().trim().required('Please enter price.'),
    offerPrice: yup
      .string()
      .nullable()
      .trim()
      .required('Please enter offer price.')
      .matches(
        numberDecimalRegExp,
        'Please enter numeric value or decimal value.',
      ),
  });

  /** Add New Product  */
  const handleProductEntry = async values => {
    console.log(values);

    setIsLoading(true);
    Keyboard.dismiss();
    try {
      await productsCollection.add(values);
      navigation.navigate(HOME);
    } catch (error) {
      setIsLoading(false);
    }
  };

  /**  Update Product Details  */
  const handleUpdateProduct = async values => {
    setIsLoading(true);
    Keyboard.dismiss();
    try {
      await productsCollection.doc(productId).update(values);
      navigation.navigate(HOME);
    } catch (error) {
      setIsLoading(false);
    }
  };

  /*****  Open Camera or Library  ****/
  const launchCameraLibrary = async type => {
    try {
      const response: any = await launchDeviceCameraLibrary(type);
      console.log(response);

      let imageObj: any = {
        uri: response.assets[0].uri,
        type: response.assets[0].type,
        name: response.assets[0].fileName,
      };
      setImageUri(response.assets[0].uri);
      // const uri = response.assets[0].uri.replace('file://', '');
      formRef.current.setFieldValue(
        'image',
        firestore.Blob.fromBase64String(response.assets[0].base64),
      );
      setCameraGalleryVisible(false);
    } catch (error) {
      setCameraGalleryVisible(false);
    }
  };
  /**  During Update fill the farm   **/
  useEffect(() => {
    if (productId) {
      formRef.current.setFieldValue('name', data.name);
      formRef.current.setFieldValue('price', data.price);
      formRef.current.setFieldValue('offerPrice', data.offerPrice);
    }
  }, []);

  return (
    <View style={commonStyle.container}>
      {isLoading && <Loader />}

      <Header title={productId ? 'Edit Product' : 'Add Product'} />
      <View style={commonStyle.formWrapper}>
        <Formik
          initialValues={initialValues}
          onSubmit={productId ? handleUpdateProduct : handleProductEntry}
          validationSchema={validationSchema}
          innerRef={formRef}>
          {({
            values,
            errors,
            touched,
            handleBlur,
            handleChange,
            handleSubmit,
          }) => (
            <ScrollView
              showsVerticalScrollIndicator={false}
              keyboardShouldPersistTaps="handled">
              <Input
                label={'Product Name'}
                placeholder={'Product Name'}
                wrapper={commonInputStyle.wrapper}
                inputWrapper={commonInputStyle.inputWrapper}
                required={true}
                labelStyle={{color: colors.black}}
                value={values.name}
                onChangeText={handleChange('name')}
                onBlur={handleBlur('name')}
                error={errors.name}
                touched={touched.name}
              />

              <Input
                label={'Price'}
                placeholder={'Price'}
                wrapper={commonInputStyle.wrapper}
                inputWrapper={commonInputStyle.inputWrapper}
                required={true}
                labelStyle={{color: colors.black}}
                keyboardType={'number-pad'}
                value={values.price}
                onChangeText={handleChange('price')}
                onBlur={handleBlur('price')}
                error={errors.price}
                touched={touched.price}
                maxLength={8}
              />

              <Input
                label={'Offer Price'}
                placeholder={'Offer Price'}
                wrapper={commonInputStyle.wrapper}
                inputWrapper={commonInputStyle.inputWrapper}
                required={true}
                labelStyle={{color: colors.black}}
                keyboardType={'number-pad'}
                value={values.offerPrice}
                onChangeText={handleChange('offerPrice')}
                onBlur={handleBlur('offerPrice')}
                error={errors.offerPrice}
                touched={touched.offerPrice}
                maxLength={8}
              />
              {imageUri && (
                <Image
                  source={{uri: imageUri}}
                  style={commonStyle.productImage}
                />
              )}
              <RippleButton
                style={commonStyle.uploadButton}
                onPress={() => {
                  setCameraGalleryVisible(true);
                }}>
                <Text style={commonStyle.uploadButtonText}>
                  Upload Product Image
                </Text>
              </RippleButton>
              <View style={commonInputStyle.buttonWrapper}>
                <CustomButton
                  title={productId ? 'Update' : 'Save'}
                  onPress={() => {
                    handleSubmit();
                  }}
                  buttonStyle={commonInputStyle.button}
                  titleStyle={{fontSize: size.font12}}
                />
                <CustomButton
                  title={'Cancel'}
                  onPress={() => {
                    navigation.navigate(HOME);
                  }}
                  buttonStyle={[
                    commonInputStyle.button,
                    commonInputStyle.cancelButton,
                  ]}
                  titleStyle={{fontSize: size.font12, color: colors.primary}}
                  rippleColor={colors.primary}
                />
              </View>

              <View style={{height: 20}} />
            </ScrollView>
          )}
        </Formik>
      </View>
      <CameraGalleryModal
        isVisible={cameraGalleryVisible}
        setIsVisible={setCameraGalleryVisible}
        openCamera={() => {
          launchCameraLibrary('camera');
        }}
        openLibrary={() => {
          launchCameraLibrary('library');
        }}
      />
    </View>
  );
};

export default AddProduct;
