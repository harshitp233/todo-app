import {FlatList, StyleSheet, View} from 'react-native';
import React, {useState, useEffect} from 'react';
import {colors} from '../../theme/colors';
import Header from '../../components/Header';
import ProductCard from '../../components/ProductCard';
import NoData from '../../components/NoData';
import CircleButton from '../../components/CircleButton';
import {useDispatch} from 'react-redux';
import auth from '@react-native-firebase/auth';
import {clearAsyncStorage, confirmAction} from '../../utils/helpers';
import {logoutUser} from '../../app/features/auth/authSlice';
import Loader from '../../components/Loader';
import firestore from '@react-native-firebase/firestore';
import {ADD_PRODUCT} from '../../constants/routeNames';
import {useIsFocused} from '@react-navigation/native';

const Home = ({navigation}: any) => {
  const productsCollection = firestore().collection('products');

  const isFocused = useIsFocused();

  const [isLoading, setIsLoading] = useState(true);

  const [products, setProducts] = useState<any[]>([]);

  const dispatch = useDispatch();

  /** Get Product List **/
  const getProducts = async () => {
    try {
      const response = await firestore().collection('products').get();
      setProducts(response.docs);
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
    }
  };

  /** Edit */
  const editProduct = (productId, data) => {
    navigation.navigate(ADD_PRODUCT, {productId, data});
  };

  /** Delete Product **/
  const deleteProduct = id => {
    confirmAction('Logout', 'Do you want to delete?', async () => {
      setIsLoading(true);
      try {
        await productsCollection.doc(id).delete();
        getProducts();
      } catch (error) {
        console.log(error);
        setIsLoading(false);
      }
    });
  };

  /** Logout user  */
  const userLogout = async () => {
    setIsLoading(true);
    try {
      await auth().signOut();
      await clearAsyncStorage();
      dispatch(logoutUser());
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
    }
  };

  /** List Item */
  const renderItem = ({item}) => {
    return (
      <ProductCard
        item={item}
        editProduct={editProduct}
        deleteProduct={deleteProduct}
      />
    );
  };

  useEffect(() => {
    getProducts();
  }, [isFocused]);

  return (
    <View style={styles.container}>
      <Header title={'Products'} logout={userLogout} isLogout={true} />
      {isLoading && <Loader />}
      <FlatList
        data={products}
        renderItem={renderItem}
        showsVerticalScrollIndicator={false}
        onEndReachedThreshold={0.5}
      />

      {/*  Info graphics */}
      {!isLoading && products.length === 0 && (
        <NoData title={'There is no product added yet.'} />
      )}

      <CircleButton
        onPress={() => {
          navigation.navigate(ADD_PRODUCT);
        }}
      />
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
});
