export const colors = {
  primary: 'rgba(41, 109, 180, 1)',
  white: 'rgba(255, 255, 255, 1)',
  black: 'rgba(44, 44, 45, 1)',
  lightBlue: 'rgba(242, 251, 255, 1)',
  grey: 'rgba(44, 44, 45, 0.3)',
  red: 'rgba(253, 87, 87, 1)',
};
