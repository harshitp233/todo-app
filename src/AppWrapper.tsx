import React from 'react';
import {SafeAreaView} from 'react-native';
import AppNavigationContainer from './navigation/AppNavigationContainer';

const AppWrapper = () => {
  return (
    <SafeAreaView style={{flex: 1}}>
      <AppNavigationContainer />
    </SafeAreaView>
  );
};

export default AppWrapper;
