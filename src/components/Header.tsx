import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import Logout from '../assets/images/svg/logout.svg';
import {colors} from '../theme/colors';
import {size, weight} from '../theme/fonts';
import {confirmAction} from '../utils/helpers';

interface props {
  title?: String;
  logout?: any;
  isLogout?: Boolean;
}
const Header = ({title, logout, isLogout = false}: props) => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>{title}</Text>
      {isLogout && (
        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            confirmAction('Logout', 'Do you want to log out?', logout);
          }}>
          <Logout />
        </TouchableOpacity>
      )}
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  container: {
    padding: 20,
    backgroundColor: colors.primary,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  wrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  iconTextWrapper: {
    alignItems: 'center',
    flex: 4,
  },
  title: {
    color: colors.white,
    fontWeight: weight.bold,
    fontSize: size.font24,
    textAlign: 'center',
  },
  iconWrapper: {
    height: 100,
    width: 100,
    borderRadius: 100 / 2,
    backgroundColor: colors.white,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    height: 40,
    width: 50,
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
});
