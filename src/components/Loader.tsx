import React from 'react';
import {ActivityIndicator, StyleSheet, View} from 'react-native';
import {colors} from '../theme/colors';
import metrics from '../theme/metrics';
const Loader = ({style = {}, color = colors.primary}) => {
  return (
    <View style={[styles.container, style]}>
      <ActivityIndicator color={color} size={'large'} />
    </View>
  );
};

export default Loader;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    width: metrics.screenWidth,
    height: metrics.screenHeight,
    backgroundColor: 'rgba(0,0,0,0.1)',
    zIndex: 1,
  },
  lottieView: {
    zIndex: 2,
    height: 50,
    width: 50,
  },
});
