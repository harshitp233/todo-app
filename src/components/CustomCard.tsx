import {View, Text, StyleSheet} from 'react-native';
import React from 'react';
import {Card} from '@rneui/base';

interface props {
  children?: any;
  wrapperStyle?: any;
  containerStyle?: any;
}
const CustomCard = ({children, containerStyle, wrapperStyle}: props) => {
  return (
    <Card
      containerStyle={[styles.containerStyle, containerStyle]}
      wrapperStyle={[styles.wrapperStyle, wrapperStyle]}>
      {children}
    </Card>
  );
};

export default CustomCard;

const styles = StyleSheet.create({
  containerStyle: {
    margin: 0,
    padding: 0,
    borderRadius: 10,
    marginVertical: 10,
    marginHorizontal: 20,
  },
  wrapperStyle: {
    padding: 10,
  },
});
