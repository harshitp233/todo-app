import {
  Platform,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import React from 'react';
import {colors} from '../theme/colors';
import {size, weight} from '../theme/fonts';

interface props {
  label?: any;
  placeholder?: any;
  LeftIcon?: any;
  RightIcon?: any;
  wrapper?: any;
  labelStyle?: any;
  input?: any;
  placeholderTextColor?: any;
  inputWrapper?: any;
  required?: any;
  value?: any;
  onChangeText?: any;
  onBlur?: any;
  error?: any;
  touched?: any;
  errorStyle?: any;
  keyboardType?: any;
  maxLength?: any;
  autoCapitalize?: any;
  multiline?: any;
  numberOfLines?: any;
  labelButton?: any;
  labelButtonPress?: any;
}
const Input = ({
  label,
  placeholder,
  LeftIcon,
  RightIcon,
  wrapper,
  labelStyle,
  input,
  placeholderTextColor,
  inputWrapper,
  required = false,
  value,
  onChangeText,
  onBlur,
  error,
  touched,
  errorStyle,
  keyboardType = 'default',
  maxLength = 50,
  autoCapitalize = 'none',
  multiline = false,
  numberOfLines = 1,
  labelButton = false,
  labelButtonPress,
}: props) => {
  return (
    <View style={[styles.wrapper, wrapper]}>
      <View style={styles.labelButtonWrapper}>
        {label && (
          <Text style={[styles.label, labelStyle]}>
            {label} {required && <Text style={{color: colors.red}}>*</Text>}
          </Text>
        )}
      </View>
      <View style={[styles.inputWrapper, inputWrapper]}>
        {LeftIcon && <LeftIcon style={{marginRight: 2}} />}

        <TextInput
          value={value}
          onChangeText={onChangeText}
          onBlur={onBlur}
          placeholder={placeholder}
          keyboardType={keyboardType}
          maxLength={maxLength}
          autoCapitalize={autoCapitalize}
          multiline={multiline}
          numberOfLines={numberOfLines}
          placeholderTextColor={
            placeholderTextColor ? placeholderTextColor : colors.grey
          }
          style={[styles.input, input]}
        />
        {RightIcon && <RightIcon style={{marginLeft: 2}} />}
      </View>
      {error && touched && (
        <Text style={[styles.error, errorStyle]}>{error}</Text>
      )}
    </View>
  );
};

export default Input;

const styles = StyleSheet.create({
  wrapper: {
    paddingVertical: 20,
  },
  input: {
    flex: 1,
    color: colors.darkGrey,
    fontSize: size.font12,
    fontWeight: weight.semi,
    padding: Platform.OS === 'ios' ? 12 : 8,
    height: 50,
  },
  label: {
    marginVertical: 5,
    color: colors.grey,
    fontSize: size.fon14,
    fontWeight: weight.semi,
  },
  inputWrapper: {
    borderWidth: 1,
    borderColor: colors.lightGrey,
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10,
    marginVertical: 5,
  },
  error: {
    fontWeight: weight.semi,
    fontSize: size.font10,
    color: colors.red,
    marginTop: 5,
    marginLeft: 5,
  },
  labelButtonWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});
