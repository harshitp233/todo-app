import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import Modal from 'react-native-modal';
import {colors} from '../theme/colors';
const CameraGalleryModal = ({
  isVisible,
  setIsVisible,
  openCamera,
  openLibrary,
}) => {
  /** Close Modal */
  const closeModal = () => {
    setIsVisible(false);
  };

  return (
    <Modal
      isVisible={isVisible}
      hasBackdrop={true}
      backdropOpacity={1}
      backdropColor={colors.grey}
      style={styles.modal}
      animationIn={'slideInUp'}
      animationOut={'slideOutDown'}
      animationInTiming={500}
      animationOutTiming={500}
      swipeDirection={'down'}
      onBackdropPress={closeModal}
      onBackButtonPress={closeModal}
      onSwipeComplete={closeModal}>
      <View style={styles.bottomView}>
        <View style={styles.header} />
        <TouchableOpacity
          style={styles.button}
          onPress={openCamera}
          activeOpacity={0.7}>
          <Text style={styles.buttonText}>Select From Camera</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={openLibrary}
          activeOpacity={0.7}>
          <Text style={styles.buttonText}>Select From Gallery</Text>
        </TouchableOpacity>
      </View>
    </Modal>
  );
};

export default CameraGalleryModal;

const styles = StyleSheet.create({
  modal: {
    margin: 0,
    padding: 0,
    justifyContent: 'flex-end',
  },

  header: {
    backgroundColor: colors.white,
    height: 4,
    width: 100,
    alignSelf: 'center',
    marginVertical: 10,
    borderRadius: 5,
  },
  bottomView: {
    backgroundColor: colors.primary,
    width: '100%',
    paddingVertical: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  button: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 20,
    marginVertical: 1,
  },
  buttonText: {
    color: colors.white,
    marginLeft: 10,
    fontSize: 16,
    fontWeight: '600',
  },
});
