import {StyleSheet} from 'react-native';
import React from 'react';
import RippleButton from './RippleButton';
import Plus from '../assets/images/svg/plus.svg';
import {colors} from '../theme/colors';
const CircleButton = ({onPress}) => {
  return (
    <RippleButton style={styles.button} onPress={onPress}>
      <Plus width={30} height={30} />
    </RippleButton>
  );
};

export default CircleButton;

const styles = StyleSheet.create({
  button: {
    position: 'absolute',
    bottom: 50,
    right: 50,
    height: 60,
    width: 60,
    borderRadius: 60 / 2,
    backgroundColor: colors.primary,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
