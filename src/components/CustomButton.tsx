import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import RippleButton from './RippleButton';
import {colors} from '../theme/colors';
import {size, weight} from '../theme/fonts';

const CustomButton = ({
  onPress,
  title,
  buttonStyle = {},
  titleStyle = {},
  rippleColor = colors.lightBlue,
}) => {
  return (
    <RippleButton
      style={[styles.button, buttonStyle]}
      rippleColor={rippleColor}
      onPress={onPress}>
      <Text style={[styles.text, titleStyle]}>{title}</Text>
    </RippleButton>
  );
};

export default CustomButton;

const styles = StyleSheet.create({
  button: {
    backgroundColor: colors.primary,
    width: '100%',
    alignSelf: 'center',
    paddingVertical: 16,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 15,
    shadowColor: '#fff',
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 0.29,
    shadowRadius: 4.65,
    elevation: 3,
  },
  text: {
    color: colors.white,
    fontSize: size.font14,
    fontWeight: weight.semi,
  },
});
