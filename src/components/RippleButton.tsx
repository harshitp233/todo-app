import React from 'react';
import Ripple from 'react-native-material-ripple';
import {colors} from '../theme/colors';

const RippleButton = ({
  children,
  onPress,
  style,
  rippleColor = colors.primary,
}: any) => {
  return (
    <Ripple
      onPress={onPress}
      style={style}
      rippleColor={rippleColor}
      rippleCentered={true}>
      {children}
    </Ripple>
  );
};

export default RippleButton;
