import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import NodataIcon from '../assets/images/svg/no-data.svg';
import {colors} from '../theme/colors';
import {size, weight} from '../theme/fonts';

interface props {
  title?: any;
  Icon?: any;
}

const NoData = ({title, Icon}: any) => {
  return (
    <View style={styles.wrapper}>
      <Text style={styles.title}>{title || 'No Data'}</Text>
      {Icon ? <Icon /> : <NodataIcon />}
    </View>
  );
};

export default NoData;

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    height: '100%',
    width: '100%',
    alignSelf: 'center',
    zIndex: -1,
  },
  title: {
    color: colors.grey,
    fontSize: size.font14,
    fontWeight: weight.bold,
    marginBottom: 20,
  },
});
