import {Image, Text, View} from 'react-native';
import React from 'react';
import Edit from '../assets/images/svg/edit.svg';
import Delete from '../assets/images/svg/delete.svg';

import CustomCard from './CustomCard';
import RippleButton from './RippleButton';
import commonStyle from '../utils/commonStyle';
const defaultProductImage = require('../assets/images/gallery.png');

const ProductCard = ({item, editProduct, deleteProduct}) => {
  const {name, price, offerPrice} = item._data;
  console.log(item?._data?.image?._binaryString);

  return (
    <CustomCard>
      <View style={commonStyle.wrapper}>
        <View style={{maxWidth: '90%'}}>
          <View style={commonStyle.productNameImageWrapper}>
            <Image
              style={commonStyle.image}
              source={
                item?._data?.image?._binaryString
                  ? {
                      uri: `data:image/png;base64;${item._data.image._binaryString}`,
                    }
                  : defaultProductImage
              }
            />
            <Text style={commonStyle.title}>
              Name : <Text style={commonStyle.value}>{name}</Text>
            </Text>
          </View>

          <Text style={commonStyle.title}>
            Price : <Text style={commonStyle.value}>{price}</Text>
          </Text>
          <Text style={commonStyle.title}>
            Offer Price : <Text style={commonStyle.value}>{offerPrice}</Text>
          </Text>
        </View>

        <View style={commonStyle.editDeleteWrapper}>
          <RippleButton
            style={[commonStyle.button]}
            onPress={() => {
              editProduct(item.id, item._data);
            }}>
            <Edit />
          </RippleButton>

          <RippleButton
            style={[commonStyle.button, {justifyContent: 'flex-end'}]}
            onPress={() => {
              deleteProduct(item.id);
            }}>
            <Delete />
          </RippleButton>
        </View>
      </View>
    </CustomCard>
  );
};

export default ProductCard;
