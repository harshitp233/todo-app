/********* Auth  ********** */

export const LOGIN = 'Login';
export const SIGN_UP = 'Sign up';

/********* Main Screen  ********** */

export const HOME = 'Main Menu';
export const ADD_PRODUCT = 'Add Product';
