import {colors} from '../theme/colors';
import {StyleSheet} from 'react-native';
import {size, weight} from '../theme/fonts';
export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.lightBlue,
  },

  formWrapper: {
    paddingHorizontal: 20,
    flex: 1,
  },

  wrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
  editDeleteWrapper: {
    justifyContent: 'space-between',
    marginVertical: 5,
  },

  title: {
    fontSize: size.font12,
    fontWeight: weight.semi,
    color: colors.grey,
    marginVertical: 5,
  },
  value: {
    fontSize: size.font12,
    fontWeight: weight.semi,
    color: colors.black,
  },
  button: {
    height: 25,
    width: 40,
    alignItems: 'flex-end',
    justifyContent: 'flex-start',
  },
  uploadButton: {
    alignItems: 'center',
    paddingVertical: 10,
  },
  uploadButtonText: {
    color: colors.primary,
    fontWeight: weight.bold,
    fontSize: size.font14,
  },
  productImage: {
    height: 100,
    width: 200,
    alignSelf: 'center',
    resizeMode: 'contain',
  },
  image: {
    height: 50,
    width: 50,
    bora: 50 / 2,
    marginRight: 10,
    resizeMode: 'contain',
  },
  productNameImageWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});

/** This style used in form input */

export const commonInputStyle = StyleSheet.create({
  wrapper: {
    backgroundColor: colors.white,
    paddingHorizontal: 10,
    borderRadius: 10,
    marginVertical: 10,
  },
  inputWrapper: {
    marginVertical: 10,
    paddingVertical: 0,
  },
  statusButtonWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    marginVertical: 10,
  },
  buttonWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    marginVertical: 30,
  },
  button: {
    width: '35%',
    paddingVertical: 10,
  },
  cancelButton: {
    backgroundColor: colors.lightBlue,
    borderWidth: 1,
    borderColor: colors.primary,
  },
});
