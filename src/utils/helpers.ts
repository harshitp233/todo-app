import AsyncStorage from '@react-native-async-storage/async-storage';

import {Alert} from 'react-native';

import {launchCamera, launchImageLibrary} from 'react-native-image-picker';

/** Get access token     */
export const getAccessToken = async () => {
  let accessToken: string | any = null;
  try {
    accessToken = await AsyncStorage.getItem('accessToken');
  } catch (e) {}
  return accessToken;
};

/**   Store access token   */
export const storeAccessToken = async (accessToken: string) => {
  try {
    await AsyncStorage.setItem('accessToken', accessToken);
  } catch (error) {}
};

/**   Clear Async Storage when user is logout   */
export const clearAsyncStorage = async () => {
  try {
    await AsyncStorage.clear();
  } catch (error) {}
};

/**   Confirm action   */
export const confirmAction = (
  title = '',
  message = '',
  action: () => void,
  cancelText = 'No',
  okayText = 'Yes',
) => {
  Alert.alert(title, message, [
    {
      text: cancelText,
      onPress: () => {},
    },
    {
      text: okayText,
      onPress: action,
    },
  ]);
};

/**** Launch Device Camera or library ******/
export const launchDeviceCameraLibrary = type => {
  const options: any = {
    mediaType: 'photo',
    cameraType: 'back',
    presentationStyle: 'overFullScreen',
    includeBase64: true,
  };
  return type === 'camera'
    ? launchCamera(options)
    : launchImageLibrary(options);
};
