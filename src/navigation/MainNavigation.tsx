import React, {useEffect} from 'react';
import {
  CardStyleInterpolators,
  createStackNavigator,
} from '@react-navigation/stack';
import {ADD_PRODUCT, HOME} from '../constants/routeNames';
import {close, open} from './navigationAnimation';
import Home from '../screens/main/Home';
import AddProduct from '../screens/main/AddProduct';

const MainNavigation = () => {
  const MainStackNavigator = createStackNavigator();

  useEffect(() => {
    return () => {};
  }, []);

  return (
    <MainStackNavigator.Navigator
      initialRouteName={HOME}
      screenOptions={{
        headerShown: false,
        gestureEnabled: true,
        gestureDirection: 'horizontal',
        transitionSpec: {
          open,
          close,
        },
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }}>
      <MainStackNavigator.Screen name={HOME} component={Home} />
      <MainStackNavigator.Screen name={ADD_PRODUCT} component={AddProduct} />
    </MainStackNavigator.Navigator>
  );
};

export default MainNavigation;
