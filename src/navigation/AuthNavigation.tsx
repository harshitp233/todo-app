import React from 'react';
import {
  CardStyleInterpolators,
  createStackNavigator,
} from '@react-navigation/stack';
import {LOGIN, SIGN_UP} from '../constants/routeNames';
import Login from '../screens/auth/Login';
import {close, open} from './navigationAnimation';
import SignUp from '../screens/auth/SignUp';

const AuthNavigation = () => {
  const AuthStackNavigator = createStackNavigator();

  return (
    <AuthStackNavigator.Navigator
      initialRouteName={LOGIN}
      screenOptions={{
        headerShown: false,
        gestureEnabled: true,
        gestureDirection: 'horizontal',
        transitionSpec: {
          open,
          close,
        },
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }}>
      <AuthStackNavigator.Screen name={LOGIN} component={Login} />
      <AuthStackNavigator.Screen name={SIGN_UP} component={SignUp} />
    </AuthStackNavigator.Navigator>
  );
};

export default AuthNavigation;
