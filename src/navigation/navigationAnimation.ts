import {Easing} from 'react-native-reanimated';

export const open: any = {
  animation: 'timing',
  config: {
    damping: 30,
    duration: 400,
    mass: 3,
    overshootClamping: false,
    restDisplacementThreshold: 0.01,
    restSpeedThreshold: 0.01,
  },
};
export const close: any = {
  animation: 'timing',
  config: {
    duration: 400,
    easing: Easing.linear,
  },
};
