import React, {useEffect, useState} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import AuthNavigation from './AuthNavigation';
import MainNavigation from './MainNavigation';
import {colors} from '../theme/colors';
import {useDispatch, useSelector} from 'react-redux';
import {RootState} from '../app/store';
import {loginUser} from '../app/features/auth/authSlice';
import auth from '@react-native-firebase/auth';
import Loader from '../components/Loader';
import {StatusBar} from 'react-native';

const AppNavigationContainer = () => {
  const dispatch = useDispatch();

  const {accessToken} = useSelector((state: RootState) => state.userAuth);

  const [initializing, setInitializing] = useState(true);

  // Handle user state changes
  const onAuthStateChanged = user => {
    dispatch(loginUser(user?.uid));
    if (initializing) setInitializing(false);
  };

  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber;
  }, []);

  if (initializing) return <Loader />;
  return (
    <NavigationContainer>
      {/* Status Bar */}
      <StatusBar
        barStyle={'light-content'}
        backgroundColor={colors.primary}
        animated={true}
      />
      {/* Navigation  */}
      {accessToken ? <MainNavigation /> : <AuthNavigation />}
    </NavigationContainer>
  );
};

export default AppNavigationContainer;
